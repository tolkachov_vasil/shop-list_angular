(function () {
  'use strict';

  angular
    .module('lightpoint')
    .config(config);

  /** @ngInject */
  function config($logProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
  }

})();
