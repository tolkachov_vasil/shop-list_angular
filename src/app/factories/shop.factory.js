(function () {
    'use strict';

    angular
        .module('lightpoint')
        .factory('shopFactory',
        /** @ngInject */
        function (dataFactory) {

            var shops = dataFactory.data;

            var service = {};

            service.getShops = function () {
                return shops;
            }

            service.getShop = function (shopIndex) {
                if (shopIndex === -1) { // if new shop
                    return {
                        name: '',
                        address: '',
                        work: '8.00-23.00',
                        id: getNewShopId(),
                        items: []
                    };
                } else {
                    return shops[shopIndex];
                }
            }


            service.deleteShop = function (shopIndex) {
                shops.splice(shopIndex, 1);
            }

            service.updateShop = function (shopIndex, newData) {
                if (shopIndex === -1) {
                    shops.push(angular.copy(newData));
                } else {
                    shops[shopIndex] = angular.copy(newData);
                }
            }

            var getNewShopId = function () {
                var max = -1;
                for (var i = 0; i < shops.length; i++) {
                    max = Math.max(shops[i].id, max);
                }
                return max + 1;
            }

            return service;

        });

})();