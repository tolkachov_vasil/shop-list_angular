(function () {
    'use strict';

    angular
        .module('lightpoint')
        .factory('dataFactory',
        function () {
            var service={};

            service.data= [
                {
                    name: 'Алми',
                    address: 'Минск, Космонавтов 28',
                    work: '9.00-23.00',
                    id: 1,
                    items: [
                        {
                            name: 'Телевизор LED DAEWOO L32R630VKE',
                            info: 'LCD 102 см / 40 дюймов • Full HD-1920x1080 пикс. • Smart TV • Wi-Fi встроенный'
                        },
                        {
                            name: 'Телевизор LED DAEWOO L32R630VK',
                            info: 'LCD 81 см / 32 дюйма • HD-1366x768 пикс.'
                        },
                        {
                            name: 'Телевизор LED LG 20MT48VF-PZ',
                            info: 'LCD TN 50 см / 20 дюймов • HD-1366x768 пикс.'
                        },
                        {
                            name: 'Телевизор LED LG 43LH570V',
                            info: 'LCD IPS 109 см / 43 дюйма • Full HD-1920x1080 пикс. • Smart TV • Wi-Fi встроенный'
                        },
                        {
                            name: 'Телевизор LG 24MT48S-PZ',
                            info: 'LCD WVA 61 см / 24 дюйма • HD-1366x768 пикс. • Smart TV • Wi-Fi'
                        },
                        {
                            name: 'Телевизор LED LG 32LH570U',
                            info: 'LCD 81 см / 32 дюйма • HD-1366x768 пикс. • Smart TV • Wi-Fi встроенный'
                        },
                        {
                            name: 'Телевизор led LG 43LF510V',
                            info: 'LCD IPS 109 см / 43 дюйма • Full HD-1920x1080 пикс.'
                        },
                        {
                            name: 'Телевизор led LG 42LF560V',
                            info: 'LCD 107 см / 42 дюйма • Full HD-1920x1080 пикс.'
                        },
                        {
                            name: 'Телевизор led LG 42LF550V',
                            info: 'LCD 107 см / 42 дюйма • Full HD-1920x1080 пикс.'
                        }
                    ]
                },
                {
                    name: 'Закрома',
                    address: 'Минск, Калиновского 72',
                    work: '9.00-22.00',
                    id: 2,
                    items: [
                        {
                            name: 'Фотокамера CANON EOS 100D EF-S 18-55 IS STM Kit',
                            info: 'Зеркальная • Видоискатель • Сенсорный экран • 3 X • 1920x1080 (FullHD) • 18 Мп • Оптическая стабилизация изображения'
                        },
                        {
                            name: 'Цифровая фотокамера CANON EOS 1300D EF-S 18-55 IS Kit',
                            info: 'Зеркальная • 1920x1080 (FullHD) • 18 Мп'
                        },
                        {
                            name: 'Фотокамера CANON EOS 1200D EF-S 18-55 III Kit',
                            info: 'Зеркальная • Видоискатель • 3 " • 3 X • 1920x1080 (FullHD)'
                        },
                        {
                            name: 'Фотоаппарат NIKON COOLPIX S7000 белый',
                            info: 'Ультразум • 20 X • 1920x1080 (FullHD) • 16 Мп • '
                        },
                        {
                            name: 'Цифровая фотокамера NIKON B500 фиолетовый',
                            info: 'Ультразум • 40 X • 1920x1080 (FullHD) • 16 Мп • Оптическая стабилизация изображения'
                        }
                    ]
                },
                {
                    name: 'Байкальский',
                    address: 'Минск, Ангарская ул. 38/2',
                    work: '8.00-23.00',
                    id: 3,
                    items: [
                        {
                            name: 'Смартфон SAMSUNG Galaxy S7 Edge Silver Titan (SM-G935FZSUSER)',
                            info: '5.5 " AMOLED (Super AMOLED QHD) • GSM (2G) EDGE (2.9G) UMTS (3G) HSPA (3.5G) HSPA+ (3.75G) LTE (4G) • Android 6.0 (Marshmallow) • Exynos 8890 • 4096 Мб • 32 ГБ • microSD (TransFlash) • Li-ion 3600 мАч'
                        },
                        {
                            name: 'Смартфон SAMSUNG Galaxy S7 Silver Titan (SM-G930FZSUSER)',
                            info: '5.1 " AMOLED (Super AMOLED QHD) • GSM (2G) EDGE (2.9G) UMTS (3G) HSPA (3.5G) HSPA+ (3.75G) LTE (4G) • Android 6.0 (Marshmallow) • Exynos 8890 • 4096 Мб • 32 ГБ • microSD (TransFlash) • Li-ion 3000 мАч'
                        },
                        {
                            name: 'Смартфон TEXET TM-4503 черный (X-quad)',
                            info: '4.5 " TFT • GSM (2G) EDGE (2.9G) UMTS (3G) • Android 4.4 (KitKat) • Spreadtrum SC7731 • 512 Мб • 4 ГБ • microSD (TransFlash) microSDHC • Li-ion 1500 мАч'
                        },
                        {
                            name: 'Мобильный телефон Oysters Kursk Black',
                            info: '1.77 " TFT • GSM (2G) • 32 МБ • microSD (TransFlash) • Li-ion 700 мАч'
                        },
                        {
                            name: 'Смартфон ASUS Zenfone Go ZB452KG Black',
                            info: '4.5 " TFT • GSM (2G) EDGE (2.9G) UMTS (3G) HSPA (3.5G) HSPA+ (3.75G) • Android 5.1 (Lollipop) • Qualcomm MSM 8212 • 1024 Мб • 8 ГБ • microSD (TransFlash) • Li-pol 2070 мАч'
                        },
                        {
                            name: 'Телефон GSM LG K5 X220DS Black Titan',
                            info: '5 " TFT • GSM (2G) EDGE (2.9G) UMTS (3G) HSPA (3.5G) HSPA+ (3.75G) • Android 5.1 (Lollipop) • MediaTek MT6582 • 1024 Мб • 8 ГБ • microSD (TransFlash) • 1900 мАч'
                        },
                        {
                            name: 'Телефон GSM LG K5 X220DS Black Gold',
                            info: '5 " TFT • GSM (2G) EDGE (2.9G) UMTS (3G) HSPA (3.5G) HSPA+ (3.75G) • Android 5.1 (Lollipop) • MediaTek MT6582 • 1024 Мб • 8 ГБ • microSD (TransFlash) • 1900 мАч'
                        },
                        {
                            name: 'Смартфон ZTE Blade L5 серый',
                            info: '5 " TFT • GSM (2G) EDGE (2.9G) UMTS (3G) HSPA (3.5G) • Android 5.1 (Lollipop) • MediaTek MT6572 • 1024 Мб • 8 ГБ • microSD (TransFlash) • Li-ion 2150 мАч'
                        },
                        {
                            name: 'Смартфон LENOVO A2010-a DUAL 3G SIM черный',
                            info: '4.5 " TFT • GSM (2G) EDGE (2.9G) UMTS (3G) HSPA (3.5G) HSPA+ (3.75G) LTE (4G) • Android 5.1 (Lollipop) • MediaTek MT6573 • 1024 Мб • 8 ГБ • Li-ion 2000 мАч'
                        }
                    ]
                },
                {
                    name: 'Белмаркет',
                    address: 'Минск, Руссиянова 1',
                    work: '9.00-23.00',
                    id: 4,
                    items: [
                        {
                            name: 'Ноутбук ASUS X540SA-XX053D',
                            info: '15.6 " TN+Film • 1366x768 пикселей Глянцевая • Intel Pentium N3700 • DDR3 4 Гб • Intel HD Graphics • HDD 500 Гб'
                        },
                        {
                            name: 'Ноутбук ASUS X540LJ-XX090D Silver',
                            info: '15.6 " TN+Film • 1366x768 пикселей Глянцевая • Intel Core-i3 4005U • DDR3 4 Гб • nVidia GeForce 920M • HDD 500 Гб'
                        },
                        {
                            name: 'Ноутбук ASUS X540LJ-XX016D Black',
                            info: '15.6 " TN+Film • 1366x768 пикселей Глянцевая • Intel Core-i3 4005U • DDR3 4 Гб • nVidia GeForce 920M • HDD 500 Гб'
                        },
                        {
                            name: 'Ноутбук Lenovo G51-35 (80M8003XUA)',
                            info: '15.6 " TN+Film • 1366x768 пикселей • AMD A6 7310 • DDR3L 4 Гб • AMD Radeon R5 M330 • HDD 500 Гб'
                        },
                        {
                            name: 'Ноутбук Lenovo G51-35 (80M8003UUA)',
                            info: '15.6 " TN+Film • 1366x768 пикселей • AMD E1 6010 • DDR3L 2 Гб • AMD Radeon R2 • HDD 500 Гб'
                        },
                        {
                            name: 'Ноутбук Acer Aspire ES1-731-P0XF NX.MZSEU.023',
                            info: '17.3 " TN+Film • 1600x900 пикселей Глянцевая • Intel Pentium N3700 • DDR3L 4 Гб • Intel HD Graphics • HDD 1000 Гб'
                        },
                        {
                            name: 'Ноутбук Lenovo IdeaPad 100-15IBD 80QQ008DUA',
                            info: '15.6 " TN+Film • 1366x768 пикселей Глянцевая • Intel Pentium 3825U • DDR3L 4 Гб • nVidia GeForce GT 920M • HDD 1000 Гб'
                        },
                        {
                            name: 'Ноутбук Acer Aspire ES1-731-C7JD (NX.MZSEU.017)',
                            info: '17.3 " TN+Film • 1600x900 пикселей Глянцевая • Intel Celeron N3050 • DDR3L 4 Гб • Intel HD Graphics • HDD 500 Гб'
                        },
                        {
                            name: 'Ноутбук ASUS X540SC-XX028T Silver',
                            info: '15.6 " TN+Film • 1366x768 пикселей Глянцевая • Intel Pentium N3700 • DDR3 4 Гб • nVidia GeForce 810M • HDD 500 Гб'
                        },
                        {
                            name: 'Ноутбук ASUS X540LJ-XX135D',
                            info: '15.6 " TN+Film • 1366x768 пикселей Глянцевая • Intel Core-i5 5200U • DDR3 6 Гб • nVidia GeForce 920M • HDD 1000 Гб'
                        },
                        {
                            name: 'Ноутбук ASUS Transformer Book T100HA-FU006T Metal 64GB (T100HA-FU006T)',
                            info: '10.1 " IPS • 1280x800 пикселей Глянцевая • Intel Atom x5-Z8500 • DDR3 2 Гб • Intel HD Graphics • eMMC 64 Гб'
                        }
                    ]
                },
                {
                    name: 'Блеск',
                    address: 'Минск, Шаранговича 51',
                    work: '8.00-22.00',
                    id: 5,
                    items: [
                        {
                            name: 'Холодильник LG GA-E409SMRL',
                            info: 'SN ST • A+ • 225 л • 87 л No Frost • 190.7 см'
                        },
                        {
                            name: 'Холодильник INDESIT DFE4200S',
                            info: 'N ST • A • 249 л • 75 л No Frost • 200 см'
                        },
                        {
                            name: 'Холодильник LG GA-E409SMRA',
                            info: 'N • A • 225 л • 87 л No Frost • 191 см'
                        },
                        {
                            name: 'Холодильник Liebherr CNef 3915',
                            info: 'Светодиодная подсветка Система автозаморозки SuperFrost Система VarioSpace в морозильной камере (для крупных продуктов) Система FrostSafe Система HomeDialog • SN T • A++ • 221 л • 119 л No Frost • 201.1 см'
                        },
                        {
                            name: 'Холодильник Liebherr CBN 4815',
                            info: 'Светодиодная подсветка Система автозаморозки SuperFrost Система VarioSpace в морозильной камере (для крупных продуктов) Система FrostSafe Система HomeDialog Система SuperCool • SN T • A+++ • 242 л • 101 л No Frost • 201 см'
                        },
                        {
                            name: 'ХОЛОДИЛЬНИК-МОРОЗИЛЬНИК ATLANT ХМ-4425-099-ND Звездная пыль',
                            info: 'N SN ST T • A • 203 л • 111 л No Frost • 206.8 см'
                        },
                        {
                            name: 'Морозильник ATLANT М-7204-190 Звездная пыль',
                            info: 'Календарь хранения продуктов • N SN ST T • A+ • 227 л Ручное • 176.5 см'
                        },
                        {
                            name: 'Холодильник LG GA-M539ZEQZ',
                            info: 'Светодиодная подсветка Интеллектуальная система диагностики (Smart Diagnostics) Система многопоточного охлаждения Multi Flow Экспресс-заморозка • N SN ST • A++ • 229 л • 109 л No Frost • 190 см'
                        },
                        {
                            name: 'Холодильник SAMSUNG RB33J3301WW/WT',
                            info: 'Светодиодная подсветка • N SN ST T • A+ • 206 л • 98 л No Frost • 185 см'
                        }

                    ]
                },
                {
                    name: 'Брест',
                    address: 'Минск, Любимова 17',
                    work: '9.00-22.00',
                    id: 6,
                    items: [
                        {
                            name: 'Стиральная машина LG FH0C3ND1',
                            info: 'Фронтальная 6 кг • Электронное • 76 дБ • A • 1000 об./мин • Блокировка от детей'
                        },
                        {
                            name: 'Стиральная машина INDESIT IWSD5085(CIS)',
                            info: 'Фронтальная 5 кг • Электронное • 72 дБ • A • 800 об./мин'
                        },
                        {
                            name: 'Стиральная машина ZANUSSI ZWSE 7120V',
                            info: 'Фронтальная 5 кг • Электронное • 78 дБ • A++ • 1200 об./мин • Блокировка от детей'
                        },
                        {
                            name: 'Стиральная машина GORENJE W6843L/S',
                            info: 'Фронтальная 6 кг • Механическое, электронное • 70 дБ • A-30 • %1400 об./мин • Блокировка от детей • Защита от перепадов напряжения'
                        },
                        {
                            name: 'Стиральная машина GORENJE W64Z02/SRIV',
                            info: 'Фронтальная 6 кг • Механическое, электронное • 68 дБ • A-20 • %1000 об./мин • Блокировка от детей • Защита от перепадов напряжения'
                        },
                        {
                            name: 'Стиральная машина ELECTROLUX EWS 1052NDU',
                            info: 'Фронтальная 5 кг • Электронное • 76 дБ • A • 1000 об./мин • Блокировка от детей'
                        },
                        {
                            name: 'Стиральная машина SAMSUNG WF60F1R1E2WDLP',
                            info: 'Фронтальная 6 кг • Электронное • 74 дБ • A • 1200 об./мин • 2 • Блокировка от детей • Защита от перепадов напряжения'
                        },
                        {
                            name: 'Стиральная машина CANDY GS4 1062D1-07',
                            info: 'Фронтальная 6 кг • Электронное • 75 дБ • A+ • 1000 об./мин • Блокировка от детей'
                        },
                        {
                            name: 'Стиральная машина LG FH2A8HDN4',
                            info: 'Фронтальная 7 кг • Электронное • 74 дБ • A • 1200 об./мин • Сенсорный • Блокировка от детей'
                        }
                    ]
                },
                {
                    name: 'Вестер',
                    address: 'Минск, Сухаревская 16',
                    work: '9.00-23.00',
                    id: 7,
                    items: [
                        {
                            name: 'Плита электрическая ZANUSSI ZCV 9553H1W',
                            info: 'Hi-Light • A 5.8 кВт • Электронное • Гриль Электрический Конвекция'
                        },
                        {
                            name: 'Плита электрическая ZANUSSI ZCV 9553G1B',
                            info: 'Hi-Light • A 7.6 кВт • Электромеханическое • Гриль Электрический Конвекция'
                        },
                        {
                            name: 'Плита электрическая ZANUSSI ZCV 9550G1X',
                            info: 'Hi-Light • A 7.6 кВт • Механическое • Гриль Электрический Конвекция'
                        },
                        {
                            name: 'Плита электрическая ELECTROLUX EKC 952301W',
                            info: 'Hi-Light • A 6 кВт • Механическое • Гриль Инфракрасный Конвекция'
                        },
                        {
                            name: 'Плита электрическая GORENJE EC55228AW',
                            info: 'Hi-Light • A 7.8 кВт • Механическое • Гриль Электрический Конвекция'
                        },
                        {
                            name: 'Плита электрическая GORENJE EC52160AW',
                            info: 'Hi-Light • A 7.7 кВт • Механическое • Гриль Электрический'
                        },
                        {
                            name: 'Плита электрическая GORENJE EC52106AW',
                            info: 'Hi-Light • A 7.8 кВт • Механическое • Гриль Электрический'
                        },
                        {
                            name: 'Электроплита BOSCH HCA644250R',
                            info: 'A 7.6 кВт • Электронное • Гриль Электрический Конвекция'
                        },
                        {
                            name: 'Электроплита GEFEST ЭП Н Д 5140-01',
                            info: 'Чугунные блины • 5.5 кВт • Механическое • Гриль Электрический'
                        }
                    ]
                },
                {
                    name: 'Волгоград',
                    address: 'Минск, Газ. Правда 40/2',
                    work: '8.00-23.00',
                    id: 8,
                    items: [
                        {
                            name: 'Электроутюг MAXWELL MW-3049BN',
                            info: 'С пароувлажнением 2000 Вт • Антипригарное покрытие • Спрей'
                        },
                        {
                            name: 'Электроутюг POLARIS PIR2466K бирюзовый',
                            info: 'С пароувлажнением 2400 Вт • Керамика • 35 г/мин • Спрей'
                        },
                        {
                            name: 'Электроутюг TEFAL FV3742E0',
                            info: 'С пароувлажнением 2000 Вт • Керамика • 30 г/мин • Спрей'
                        },
                        {
                            name: 'Утюг POLARIS PIR2258AК синий',
                            info: 'С пароувлажнением 2200 Вт • Керамика • Спрей'
                        },
                        {
                            name: 'Утюг POLARIS PIR2458AК голубой',
                            info: 'С пароувлажнением 2400 Вт • Керамика • 35 г/мин • Спрей'
                        },
                        {
                            name: 'Утюг BRAUN TS725A',
                            info: 'С пароувлажнением 2400 Вт • Eloxal • 50 г/мин • Спрей'
                        },
                        {
                            name: 'Электроутюг SATURN ST-CC0220 розовый',
                            info: 'Дорожный 1000 Вт • Нержавеющая сталь'
                        },
                        {
                            name: 'Утюг PHILIPS GC4512/20',
                            info: 'С пароувлажнением 2400 Вт • Steamglide Plus • 45 г/мин • Спрей'
                        },
                        {
                            name: 'Утюг REDMOND RI-C244 оранжевый',
                            info: 'С пароувлажнением 2200 Вт • Керамика • 50 г/мин • Спрей'
                        }
                    ]
                },
                {
                    name: 'Евроопт',
                    address: 'Минск, Калиновского 23',
                    work: '9.00-23.00',
                    id: 9,
                    items: [
                        {
                            name: 'Кухонный комбайн Bosch MUM4875EU',
                            info: '600 Вт • Импульсный режим • Пластик • Крюк для теста Блендер Блендер Мясорубка Венчик Для теста Для взбивания'
                        },
                        {
                            name: 'Кухонная машина Kenwood KVC5000T',
                            info: '1100 Вт • Импульсный режим • Алюминий • Лопаточка Крюк для теста К-образная насадка Венчик'
                        },
                        {
                            name: 'Кухонный комбайн REDMOND RFP-CB3910',
                            info: '1350 Вт • Пластик • Мерный стакан Диск-терка Ножевой блок S-образной формы Блендер Измельчитель со съемным ножом Венчик Насадка для пюре'
                        },
                        {
                            name: 'Кухонный комбайн REDMOND RFP-3950',
                            info: '1000 Вт • Пластик • Универсальный нож Диск для нарезки ломтиками Блендер Измельчитель со съемным ножом Венчик'
                        },
                        {
                            name: 'Кухонный комбайн Bosch MCM3110W',
                            info: '800 Вт • Импульсный режим • Пластик • Мерный стакан-толкатель Многофункциональный нож из нержавеющей стали'
                        },
                        {
                            name: 'Кухонный комбайн HOLT HT-FP-006',
                            info: '250 Вт • Импульсный режим • Пластик • Мелкая терка Крупная терка Насадка для слайсов Измельчитель со съемным ножом'
                        },
                        {
                            name: 'Кухонный комбайн BOSCH MCM3401M',
                            info: '800 Вт • Импульсный режим • Металл Пластик • Мелкая шинковка Крупная шинковка Диск для взбивания Измельчитель со съемным ножом Для теста'
                        },
                        {
                            name: 'Кухонный комбайн BRAUN FP3010',
                            info: '600 Вт • Импульсный режим • Пластик • Мелкая терка Крупная терка Насадка для взбивания'
                        },
                        {
                            name: 'Кухонный комбайн BRAUN FP5160BK',
                            info: '1000 Вт • Импульсный режим • Пластик • Насадка для взбивания Блендер Соковыжималка Пресс-цитрус'
                        }
                    ]
                },
                {
                    name: 'Живинка',
                    address: 'Минск, Ландера 36А',
                    work: '8.00-23.00',
                    id: 10,
                    items: [
                        {
                            name: 'Мультиварка Redmond SkyCooker RMC-M92S',
                            info: '1000 Вт • ТЭН 5 л • Сенсорное • Таймер • Отсрочка приготовления • Мультиповар • 3D-разогрев'
                        },
                        {
                            name: 'Мультикухня Redmond RMK-M271',
                            info: '830 Вт • ТЭН 5 л • Сенсорное • Таймер • Отсрочка приготовления • Мультиповар'
                        },
                        {
                            name: 'Мультиварка Redmond RMC-M40S',
                            info: '700 Вт • ТЭН 5 л • Сенсорное • Таймер • Отсрочка приготовления • Мастершеф Лайт Мультиповар'
                        },
                        {
                            name: 'Мультиварка REDMOND RMC-PM380',
                            info: '1000 Вт • 6 л • Сенсорное • Таймер • Отсрочка приготовления • Мастершеф'
                        },
                        {
                            name: 'Мультиварка REDMOND RMC-M4510(белый)',
                            info: '700 Вт • ТЭН 5 л • Электронное • Таймер • Отсрочка приготовления • Мастершеф Мультиповар'
                        },
                        {
                            name: 'Мультиварка REDMOND RMC-M800S',
                            info: '900 Вт • ТЭН 5 л • Сенсорное • Таймер • Отсрочка приготовления • Мастершеф Мультиповар • 3D-разогрев'
                        },
                        {
                            name: 'Мультиварка REDMOND RMC-M4510(черный)',
                            info: '700 Вт • ТЭН 5 л • Электронное • Таймер • Отсрочка приготовления • Экспресс Мультиповар'
                        },
                        {
                            name: 'Мультиварка REDMOND RMC-250',
                            info: '860 Вт • ТЭН 4 л • Сенсорное • Таймер • Отсрочка приготовления • Экспресс Мой рецепт Мультиповар • 3D-разогрев'
                        },
                        {
                            name: 'Мультиварка REDMOND RMC-M90',
                            info: '860 Вт • ТЭН 5 л • Сенсорное • Таймер • Отсрочка приготовления • Мультиповар • 3D-разогрев'
                        }
                    ]
                },
                {
                    name: 'Зеленый Клен',
                    address: 'Минск, Руссиянова 6',
                    work: '9.00-22.00',
                    id: 11,
                    items: [
                        {
                            name: 'Микроволновая печь LG MS20C44D',
                            info: '20 л Эмаль • 700 Вт • Сенсорные • 455 мм 284 мм 312 мм'
                        },
                        {
                            name: 'Печь микроволновая MIDEA AM720C4E-W',
                            info: '20 л Smart Clean • 1050 Вт • Кнопочные • 439.5 мм 258.2 мм 336 мм'
                        },
                        {
                            name: 'Печь микроволновая MIDEA TG025LX3',
                            info: '25 л Антибактериальное Эмаль • 1550 Вт • Гриль • 513 мм 327.7 мм 378 мм'
                        },
                        {
                            name: 'Печь микроволновая MIDEA AC925N3A',
                            info: '25 л Эмаль SmartCeramics • Гриль • Кнопочные • 435 мм 258 мм 357 мм'
                        },
                        {
                            name: 'Печь микроволновая MIDEA AM720C3P-C',
                            info: '20 л Smart Clean Эмаль • 1050 Вт • Сенсорные • 440 мм 260 мм 347 мм'
                        },
                        {
                            name: 'Печь микроволновая бытовая MIDEA MM820CXX-W',
                            info: '20 л Smart Clean • 1270 Вт • Поворотные • 440 мм 260 мм 358 мм'
                        },
                        {
                            name: 'Печь микроволновая бытовая MIDEA MM720C4E-S',
                            info: '20 л Smart Clean Эмаль • 1050 Вт • Поворотные • 439.5 мм 258.2 мм 336 мм'
                        },
                        {
                            name: 'Печь микроволновая MIDEA MM820CFB-S',
                            info: '20 л Эмаль • Поворотные • 440 мм 258 мм 345 мм'
                        },
                        {
                            name: 'Микроволновая печь PANASONIC NN-ST271SZTE',
                            info: '19 л Эмаль • 1250 Вт • Сенсорные • 440 мм 260 мм 330 мм'
                        }
                    ]
                },
                {
                    name: 'Кампарис',
                    address: 'Минск, Жудро 16',
                    work: '9.00-23.00',
                    id: 12,
                    items: [
                        {
                            name: 'Электронная книга RITMIX RBK-675FL',
                            info: 'Rockchip 2818 800 МГц Пластик • E Ink Carta 6 " • 128 Мб • microSD microSDHC • MOBI EPUB FB2 RTF DJVU HTML TXT PDF PDB HTM'
                        },
                        {
                            name: 'Электронная книга POCKETBOOK 626 (Touch Lux 3) Серый',
                            info: '1000 МГц Пластик • E Ink Carta 6 " • Сенсорный экран • 256 Мб • microSD • FB2.ZIP EPUB PDF DRM PRC TCR DOC FB2 RTF DJVU HTML CHM TXT PDF EPUB DRM DOCX • Wi-Fi'
                        },
                        {
                            name: 'Электронная книга TEXET TB-137SE 4 ГБ, серый',
                            info: 'Пластик • E-Ink Pearl 6 " • microSD microSDHC • MOBI EPUB DOC FB2 RTF DJVU HTML CHM TXT PDF PDB'
                        },
                        {
                            name: 'Электронная книга POCKETBOOK 630 (Sense with KENZO cover) серый',
                            info: '1000 МГц Пластик • E-Ink Pearl 6 " • Сенсорный экран • 256 Мб • microSD • MOBI FB2.ZIP EPUB PRC TCR DOC FB2 RTF ACSM DJVU HTML CHM TXT PDF EPUB DRM DOCX HTM • Wi-Fi'
                        },
                        {
                            name: 'Электронная книга POCKETBOOK 650 (Ultra) Изумрудный',
                            info: '1000 МГц Пластик • E Ink Carta 6 " • Емкостный мультисенсорный • 512 Мб • microSD microSDHC • MOBI EPUB PRC TCR DOC FB2 RTF DRM DJVU HTML CHM TXT PDF DOCX • MP3 • Wi-Fi'
                        },
                        {
                            name: 'Электронная книга POCKETBOOK 624 (Basic Touch) Серый',
                            info: '1000 МГц Пластик • E-Ink Pearl 6 " • Сенсорный экран • 256 Мб • microSD • MOBI EPUB PRC TCR DOC FB2 RTF DRM DJVU CHM TXT PDF DOCX HTM • Wi-Fi'
                        },
                        {
                            name: 'Электронная книга POCKETBOOK 624 (Basic Touch) Белый',
                            info: '1000 МГц Пластик • E-Ink Pearl 6 " • Сенсорный экран • 256 Мб • microSD • MOBI EPUB PRC TCR DOC FB2 RTF DRM DJVU HTML CHM TXT PDF DOCX HTM • Wi-Fi'
                        },
                        {
                            name: 'Электронная книга POCKETBOOK 614 (Basic 2) Серый',
                            info: '1000 МГц Пластик • E-Ink Pearl 6 " • 256 Мб • microSD • MOBI EPUB PRC TCR DOC FB2 RTF ACSM DJVU HTML CHM TXT PDF DOCX HTM'
                        },
                        {
                            name: 'Электронная книга POCKETBOOK 614 (Basic 2) Белый',
                            info: '1000 МГц Пластик • E-Ink Pearl 6 " • 256 Мб • microSD • MOBI EPUB PRC TCR DOC FB2 RTF ACSM DJVU HTML CHM TXT PDF DOCX HTM'
                        }
                    ]
                }
            ];

            return service;

        });

})();