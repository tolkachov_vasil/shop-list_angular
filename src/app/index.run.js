(function() {
  'use strict';

  angular
    .module('lightpoint')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
