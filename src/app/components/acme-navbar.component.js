(function () {
  'use strict';

  angular
    .module('lightpoint')
    .component('acmeNavbar', {
      templateUrl: 'app/partials/acme-navbar.html'
    })

})();
