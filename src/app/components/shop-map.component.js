(function () {
    'use strict';

    angular
        .module('lightpoint')
        .component('shopMap', {
            templateUrl: 'app/partials/shop-map.html',
            /** @ngInject */
            controller: function (shopFactory, NgMap, $state) {
                var $ctrl = this;
                $ctrl.shops = shopFactory.getShops();

                $ctrl.isShowMap = function () {
                    return $state.$current.name === 'home';
                };

                NgMap.getMap().then(function (map) {
                    $ctrl.map = map;
                });
            }
        })

})();