(function () {
    'use strict';

    angular
        .module('lightpoint')
        .component('shopEdit', {
            templateUrl: 'app/partials/shop-edit.html',
            /** @ngInject */
            controller: function (shopFactory, $stateParams, $state, $uibModal) {
                var $ctrl = this;
                $ctrl.editItemIndex = -1;
                $ctrl.draggedItemIndex = -1;
                $ctrl.shopIndex = parseInt($stateParams.shopIndex, 10);
                $ctrl.shop = shopFactory.getShop($ctrl.shopIndex);
                $ctrl.edit = angular.copy($ctrl.shop);

                $ctrl.editItem = function (itemIndex) {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: 'app/partials/item-edit.modal.html',
                        controller: 'itemEditModalController',
                        controllerAs: 'vm',
                        size: 'lg',
                        resolve: {
                            item: function () {
                                $ctrl.editItemIndex = itemIndex;
                                if (itemIndex === -1) { // if new item
                                    return { name: '', info: '' };
                                } else {
                                    return $ctrl.edit.items[itemIndex];
                                }
                            }
                        }
                    });

                    modalInstance.result.then(function (newItemValue) {
                        if ($ctrl.editItemIndex === -1) { // if new item
                            $ctrl.edit.items.push(newItemValue);
                        } else {
                            $ctrl.edit.items[$ctrl.editItemIndex] = newItemValue;
                        }
                    });
                };

                $ctrl.updateShop = function () {
                    shopFactory.updateShop($ctrl.shopIndex, $ctrl.edit);
                    $state.go('home');
                };

                $ctrl.deleteItem = function (itemIndex) {
                    $ctrl.edit.items.splice(itemIndex, 1);
                };

                $ctrl.onDropItem = function (targetIndex, droppedItem) {
                    $ctrl.edit.items.splice($ctrl.draggedItemIndex, 1);
                    $ctrl.edit.items.splice(targetIndex, 0, droppedItem);
                }

            }
        })

})();
