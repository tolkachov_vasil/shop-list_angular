(function () {
    'use strict';

    angular
        .module('lightpoint')
        /** @ngInject */
        .controller('itemEditModalController',
        /** @ngInject */
        function ($uibModalInstance, item) {
            var vm = this;

            vm.edit = angular.copy(item);

            vm.ok = function () {
                $uibModalInstance.close(vm.edit);
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

        })

})();
