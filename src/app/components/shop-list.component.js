(function () {
    'use strict';

    angular
        .module('lightpoint')
        .component('shopList', {
            templateUrl: 'app/partials/shop-list.html',
            /** @ngInject */
            controller: function (shopFactory) {
                var $ctrl = this;
                $ctrl.draggedShopIndex = -1;
                $ctrl.shops = shopFactory.getShops();

                $ctrl.deleteShop = function (shopIndex) {
                    shopFactory.deleteShop(shopIndex);
                }

                $ctrl.onDrop = function (targetIndex, droppedShop) {
                    $ctrl.shops.splice($ctrl.draggedShopIndex, 1);
                    $ctrl.shops.splice(targetIndex, 0, droppedShop);
                }

            }
        })

})();
