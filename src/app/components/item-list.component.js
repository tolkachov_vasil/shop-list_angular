(function () {
    'use strict';

    angular
        .module('lightpoint')
        .component('itemList', {
            templateUrl: 'app/partials/item-list.html',
            /** @ngInject */
            controller: function (shopFactory, $stateParams) {
                var $ctrl = this;
                $ctrl.shopIndex = parseInt($stateParams.shopIndex, 10);
                $ctrl.shop = shopFactory.getShop($ctrl.shopIndex);
            }
        })

})();
