(function () {
  'use strict';

  angular
    .module('lightpoint')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        template: '<shop-list></shop-list>'
      })
      .state('shop', {
        url: '/shop/:shopIndex',
        template: '<item-list></item-list>'
      })
      .state('shopEdit', {
        url: '/shop/edit/:shopIndex',
        template: '<shop-edit></shop-edit>'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
