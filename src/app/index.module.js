(function () {
  'use strict';

  angular
    .module('lightpoint', ['ui.router', 'ui.bootstrap', 'ngMap', 'dndLists']);

})();
